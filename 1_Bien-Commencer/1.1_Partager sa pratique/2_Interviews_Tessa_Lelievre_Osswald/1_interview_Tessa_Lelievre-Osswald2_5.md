# Tessa Lelièvre-Osswald 2/5, enseigner l'informatique au secondaire

Cette série d'interviews a pour but de recueillir l'expérience d'enseignants et d'enseignantes de NSI (spécialité Numérique et Sciences Informatiques):
comment sont-ils arrivés à enseigner cette discipline, quelles difficultés ont-ils rencontrées, quelle pédagogie ont-ils mis en oeuvre, ...  ? Ils et elles témoignent pour partager leurs pratiques avec de jeunes venus dans l'enseignement de NSI.

## Interview de Tessa Lelièvre-Osswald, enseignante en NSI

**Sommaire des 5 vidéos**

* [1/5 Tessa Lelièvre-Osswald, qui es-tu et quel est ton parcours ?](./1_interview_Tessa_Lelievre-Osswald1_5.md) 
* **2/5 Comment se former et préparer ses cours ?**
* 3/5 [Pratique en classe\.](./1_interview_Tessa_Lelievre-Osswald3_5.md)
* 4/5 [Enseignement adapté aux différents élèves\.](./1_interview_Tessa_Lelievre-Osswald4_5.md)
* 5/5 [Conseils aux autres collègues\.](./1_interview_Tessa_Lelievre-Osswald5_5.md)


## 2/5 Comment se former et préparer ses cours ? 

_"D’abord j’avais de bonnes bases des enseignements reçus en licence et master, très utiles, et pour passer l'épreuve du CAPES, j'ai commencé à écrire mes cours, en partant du principe que je l'avais, et préparer mes séquences comme si j'allais enseigner, pour les trois niveaux SNT en seconde et NSI en première et terminale, donc pas un cours complet mais avoir une bonne base qui a fortement facilité l’épreuve du CAPES..."_

[![Interview Tessa Lelièvre-Osswald 2/5](https://mooc-nsi-snt.gitlab.io/portail/assets/vignette-interview-Tessa-600x311.png)](https://files.inria.fr/LearningLab_public/C045TV/ITW/NSI-ITW-TL-2.mp4)


### Se préparer et préparer ses cours : par soi-même.

_Alors, comment t'es tu formée et préparée à enseigner, en particulier au niveau du CAPES ?_

Déjà, j'avais quand même de bonnes bases, on va dire des enseignements que j'ai reçu en licence et en master qui m'ont été très utiles. Et pour vraiment passer l'épreuve du CAPES, en fait, j'ai commencé à écrire mes cours en partant du principe que j'allai réussir, donct préparer mes séquences comme si j'allai enseigner. Ceci pour les trois niveaux seconde, première terminale en SNT et NSI, donc pas un cours complet, mais avoir une bonne base, ce qui a fortement facilité l'épreuve du CAPES, surtout la partie sur la pédagogie. Et ensuite, quand j'ai eu à créer mes cours, j'ai pu reprendre ces bases et juste les compléter.

_Alors, pour préparer ces cours, pour le CAPES et pour ensuite, tu t'es appuyé sur quoi ?_

En fait, j'ai essayé au maximum de tout créer par moi même, puisque bien qu'il y ait quelques ressources intéressantes, il n'y en a quand même pas énormément. Il n'y avait pas tellement de manuel, vu que c'était le début de cette matière. Je me suis quand même dit que le cours se passerait de manière beaucoup plus fluide, si le contenu venait de moi. Donc j'ai fait un maximum par moi même. Bien sûr, en se basant sur les cours que j'ai reçus, ça m'a influencé et il y a quelques notions où j'ai réutilisé la manière dont mes enseignants me l'avait transmise pour le transmettre aux élèves. Mais j'ai quand même essayé de faire un maximum par moi même pour pouvoir après le faire de la manière la plus fluide possible devant les élèves.

_C'est très intéressant, mais dis moi cet enseignement universitaire, comment l'as tu adapté au niveau lycée, première et terminale ?_

Alors, ce n'est pas forcément évident, en effet, puisqu'on a des éléments du programme qui correspondent à ce qu'on est censé faire en licence, voire même en master. Donc, bien évidemment, on va plus lentement, en donnant plus d'exemples, en simplifiant les choses des fois, même dans les explications. Pour commencer, par des choses plus évidentes et après essayer de généraliser. On généralise bien sur moins qu'en université, mais selon le niveau des élèves, il y en a qui arrivent à comprendre le global, assez bien. Donc voilà. Mais c'est surtout en commençant par des exemples et des simplifications des informations avant de vraiment rentrer dedans.




