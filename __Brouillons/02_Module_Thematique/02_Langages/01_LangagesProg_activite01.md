# Langages et Programmation

## Activité 1 : analyse d'une fiche concrète

Vous disposez de [activite_01](01_LangagesProg_activite01.pdf)

1. Quels sont les connaissances visées par cette activité ?
2. A quels niveaux d’enseignement cette activité pourrait être proposée ?
3. Déterminer les intentions de l’auteur pour chacun des exercices ?
4. Déterminer les pré-requis nécessaires à cette activité.
5. Vous décidez d’utiliser cette activité avec vos élèves. Écrire le scénario de la séance.

Exercices dérivés :

## Exercice 2 : Remédiation

Certains élèves ont été en difficulté sur les exercices 12, 13 et 14.
1. Proposer une activité de remédiation utilisant le module `Turtle` visant les mêmes objectifs.
2. Proposer une activité de remédiation n’utilisant pas le module `Turtle` visant les mêmes objectifs.


## Exercice 3 : Gestion de l’hétérogénéïté
Certains élèves ont été particulièrement en réussite sur cette activité.
1. Proposer une activité utilisant le module `Turtle` visant les mêmes objectifs pour ces élèves.
2. Proposer une activité n’utilisant pas le module `Turtle` visant les mêmes objectifs pour ces élèves


## Exercice 3 : Vers la transposition
Quelques semaines plus tard vous décidez d’approfondir les connaissances travaillées dans cette activité.
1. Proposer une activité appelée « activite_02 » utilisant le module `Turtle` visant les mêmes connaissances
2. Proposer une activité « activite_03 » n’utilisant pas le module `Turtle` visant les mêmes connaissances.

## Exercice 4 : Évaluations
1. Proposer une évaluation formative qui aurait lieu entre les séances consacrées aux activités « activite_01 » et « activite_02/03 ».
2. Proposer une évaluation sommative qui pourrait avoir lieu après la séance consacrée aux activités « activite_02/03 ».