---
title: 15. Programmes dirigés
---

# Programmes dirigés


Aucun ordinateur ne pourrait fonctionner sans système d'exploitation.
Les futurs techniciens d'assistance IT doivent donc acquérir des
connaissances approfondies dans ce domaine. Enseignant à l'école
d'enseignement professionnel, W. souhaite que ses élèves puissent
apprendre à la fois les concepts fondamentaux des systèmes
d'exploitation et leurs applications pratiques. Les connaissances
préalables de ses élèves dans ce domaine sont cependant très disparates.
Certains sont de véritables experts de Linux et installent une nouvelle
version d'un système d'exploitation en un tour de main, alors que
d'autres trouvent déjà difficile sa simple utilisation.

---

**Problème :** le spectre des connaissances préalables des étudiants
dans l'enseignement de l'informatique est souvent très large  ; il est
donc difficile d'intégrer ce facteur dans l'organisation de la
formation.


La maîtrise du cours précédent est le facteur le plus important dans la
réussite de l'apprentissage d'un nouveau sujet (voir par ex. Bloom
[Blo76]). La tentation est néanmoins grande d'aborder le thème suivant
dès qu'une partie des élèves a compris le sujet actuel, car le programme
pédagogique met les enseignants sous pression. De nombreux enseignants
attendent en fait que les premiers élèves confirment par hochement de
tête ou par un « ouais » vigoureux qu'ils ont bien compris le cours et
poursuivent alors le programme établi dès qu'environ un tiers de la
classe l'a comprise (effet Lundgren, [Lun72]). Le paradoxe ici est que
ce sont précisément les enseignants qui empêchent la majorité des élèves
de bien comprendre le sujet. La conséquence est un cercle vicieux :
moins un élève comprend un sujet, plus il lui sera difficile de
comprendre les nouveaux qui en découlent. Le risque de frustration est
ainsi élevé. Par conséquent, un enseignement de bonne qualité ne doit
pas se baser sur les 30 % d'élèves les plus rapides d'une classe, mais
s'articuler autour de la pédagogie de la maîtrise ou Mastery Learning :
l'étude du sujet ne se poursuit que lorsque 80 à 90 % des élèves ont
compris le thème traité.


Le problème qui se pose dans l'enseignement de l'informatique est que
les élèves apprennent et progressent à des rythmes différents. Phénomène
ici bien plus prononcé que dans d'autres spécialités : alors que dans la
connaissance des concepts les différences de connaissances préalables
restent généralement dans des limites raisonnables, il existe souvent
une forte disparité au niveau des compétences de manipulation de
l'ordinateur et de la connaissance des différents programmes. Les
programmes dirigés apportent ici une solution en personnalisant
l'enseignement. La méthode d'enseignement remonte au concept du plan
Keller [Kel68] et s'inspire du principe Mastery Learning de Benjamin
Bloom. Le thème est divisé en paquets individuels et s'accompagne
d'instructions pour l'auto-apprentissage. Un test est effectué auprès de
l'enseignant après chaque paquet, et ce n'est qu'après avoir réussi ce
test que le paquet suivant peut être entamé. Les étudiants doivent
comprendre le sujet avant de pouvoir continuer (Fig. 15.1).


Traitement du sujet au cours d'un script de 30 à 90 minutes

| Contrôle du résultat : ai-je tout compris ? | 
| -----------| 
| *oui* |
| *non* |

**Fig. 15.1** Principe Mastery Learning avec les programmes dirigés

Un programme dirigé doit inclure toutes les informations essentielles
pour les étudiants et est généralement constitué de la manière illustrée
dans la figure 15.2. Un programme dirigé tient compte des connaissances
préalables et des rythmes d'apprentissage différents des élèves en
incluant à cet effet un fundamentum et un additum. Le fundamentum
contient les parties du sujet que tout le monde doit être en mesure de
maîtriser. L'additum est conçu pour les élèves les plus rapides et
contient un complément plus approfondi. Ce complément doit être
attrayant pour que les plus rapides l'étudient. Un additum bien adapté à
l'informatique se compose d'informations sur les produits actuels, de
différentes solutions techniques appliquées, d'autres variantes de
solution, de considérations supplémentaires relatives au temps
d'exécution des algorithmes ou encore de courtes expériences.


Chaque chapitre d'un programme dirigé commence par une vue d'ensemble et
la présentation des objectifs d'apprentissage, suivis d'une introduction
facilement compréhensible du sujet. Un programme dirigé bien conçu ne se
contente pas de présenter les sujets par de longs textes, mais il est
varié et encourage les étudiants à faire preuve d'initiative et à
réfléchir par eux-mêmes.

| **Rythme individuel** |  **Contenu**  | 
| -----------| -----------| 
| **Fundamentum** <p>Tous les élèves doivent en acquérir au moins 80 % </p> | Présentation générale et objectifs <p>**Présentation facilement compréhensible du sujet**</p> <p>Exercices sur le sujet, instructions pour des expériences</p> |
| **Additum** <p> Absence de temps mort pour les plus rapides </p> | **Contrôle des connaissances (Tests)** <p> Annexes (solutions des exercices) </p> | 


**Fig. 15.2** Construction d'un chapitre d'un programme dirigé


En informatique, un programme dirigé permet, pour de nombreux sujets,
d'intégrer de petits exercices sur ordinateur. Exemple : pendant le
cours sur les réseaux, découvrir soi-même les différents aspects du
fonctionnement du réseau de l'école ou, pendant les travaux pratiques,
approfondir directement sur l'ordinateur les principes de la feuille de
calcul. Pour que les élèves puissent travailler de manière autonome, il
convient de leur donner des solutions détaillées pour tous les
exercices.


Chaque chapitre est suivi d'un contrôle des connaissances : l'élève
vérifie lui-même s'il a bien compris le sujet. L'enseignant fait ensuite
passer un test, par écrit ou au cours d'un entretien oral. Ce test est
important, car il donne aux étudiants la possibilité de clarifier
certaines ambiguïtés directement avec l'enseignant. C'est ce contact
intensif et direct avec l'enseignant qui distingue la méthode du
programme dirigé de toutes les autres formes d'auto-apprentissage.


Les programmes dirigés sont adaptés à des groupes hétérogènes
d'étudiants ayant des connaissances préalables différentes et aux sujets
complexes. Dans les deux cas, la personnalisation tient compte des
rythmes d'apprentissage différents : les stagiaires les plus lents
peuvent prendre leur temps et éventuellement revenir en arrière tandis
que les plus rapides peuvent sauter les compléments d'explication et
découvrir des aspects supplémentaires dans l'additum. Il existe un
avantage supplémentaire dans le cas des sujets difficiles qui peuvent
ainsi être présentés plus précisément par écrit que verbalement. La
confiance en soi des étudiants est en outre renforcée lorsqu'ils
réussissent à accomplir par eux-mêmes une tâche difficile.


Cependant, les programmes dirigés présentent également des inconvénients
dont le principal, du point de vue de l'enseignant, est l'effort
nécessaire à leur préparation. Le langage pédagogique verbal laisse
beaucoup de liberté d'improvisation pendant le cours. Dans le cas d'un
programme dirigé, l'enseignant doit prévoir chaque détail, se demander
quels aspects du sujet sont particulièrement difficiles et nécessitent
de ce fait une présentation détaillée. Par conséquent, les programmes
dirigés devraient en priorité être créés pour des thèmes récurrents et
conjointement par plusieurs enseignants. Du point de vue des étudiants,
les programmes dirigés impliquent qu'ils doivent prendre en charge leur
propre apprentissage et ne peuvent pas se cacher au fond de la classe.
Ce sont toutefois ces inconvénients qui font des programmes dirigés
l'une des méthodes pédagogiques les plus efficaces : le sujet est
présenté avec soin ; les étudiants sont donc contraints de l'étudier de
manière intensive.


**Solution :** les connaissances préalables des produits et les
compétences dans leur manipulation sont très hétérogènes dans
l'enseignement de l'informatique. Toute forme d'enseignement centrée sur
l'enseignant impose un rythme d'apprentissage commun inadapté à de
nombreux étudiants, dans un sens ou dans l'autre. La solution réside
dans des formes pédagogiques plus personnalisées, centrées sur les
étudiants. Les programmes dirigés sont des matériels
d'auto-apprentissage incluant 2 à 10 cours et convenant parfaitement à
l'enseignement de l'informatique.


**Exemple 1 : programme dirigé d'introduction au système d'exploitation
Unix**


Un système d'exploitation comme Unix contient énormément d'instructions.
La simple énumération des instructions une à une avec description des
options associées n'aurait bien évidemment aucun sens du point de vue
pédagogique. Il est nettement préférable ici que les étudiants puissent
se familiariser avec les principales instructions et l'utilisation des
pages d'aide. Des exercices pratiques sur ordinateur sont donc
nécessaires. La Figure 15.3 est un exemple de chapitre d'un programme
dirigé correspondant [Swi] où une présentation générale et les
objectifs de la formation sont suivis d'une courte partie théorique.
Cette dernière est illustrée par des exercices pratiques sur ordinateur.
Après la partie théorique viennent des applications destinées à
approfondir le sujet traité.


Cet exemple illustre également la différence entre un mode d'emploi pur
et un programme dirigé. Un programme dirigé ne doit pas se limiter aux
consignes d'utilisation de l'OS UNIX, mais doit également communiquer
ses concepts fondamentaux. Après avoir réalisé le programme dirigé, les
étudiants doivent avoir une idée de ce qu'est un système d'exploitation,
de ce qui caractérise plus spécifiquement UNIX et de la façon de
travailler concrètement avec UNIX.


#### Objectifs pédagogiques

Apprendre à protéger ses fichiers ou ses répertoires contre un accès non
autorisé.

Se familiariser avec deux caractères génériques simples qui seront très
utiles dans l'avenir, notamment pour rechercher des documents.


#### Théorie

Chaque fichier et répertoire sous UNIX possède non seulement un nom,
mais aussi toute une série d'autres informations. Ces caractéristiques
spécifiques (attributs) sont enregistrées dans l'en-tête du fichier et
un grand nombre d'entre elles peut être affiché en tapant l'option -l
après la commande ls.

Si le répertoire actuel n'est pas vide, le Shell peut alors prendre
l'aspect suivant, par exemple :


#### Pratique

<img alt="Fig. 15.1. Principe Mastery Learning avec les programmes dirigés" src="Images/15.1_figure.jpg" width="450px"/>

**Applications**

1er exercice
<p>Crée un nouveau répertoire ayant pour nom « Personnel ».</p>
<p>À quoi les droits d'accès des différents groupes d'utilisateurs
ressemblent-ils à présent ?</p>

[…]

**Fig. 15.3** Extrait d'un programme dirigé d'introduction à Unix

**Exemple 2 : programme dirigé de programmation récursive**


La récursivité comme méthode de conception d'algorithmes est une
construction mentale difficile. Plusieurs essais sont généralement
nécessaires avant de comprendre le principe de la récursivité, avec pour
difficulté supplémentaire que la présentation du thème de la récursivité
dans le matériel didactique n'est souvent pas optimale. La récursivité
en tant que principe algorithmique, par exemple, est mélangée avec le
concept de répétition de séquences définies. La suite des nombres de
Fibonacci est ainsi fréquemment utilisée comme exemple d'introduction à
la programmation récursive, qui porte justement sur le calcul itératif
de séquences définies de manière récursive. Dans un programme dirigé,
les termes peuvent être clairement définis et différenciés [Swi]. Le
programme dirigé permet à l'élève de consacrer aux bases de la
récursivité le temps nécessaire pour comprendre pleinement le concept.

Un programme dirigé sur la programmation récursive peut d'abord
présenter le principe de la récursivité en utilisant des exemples
simples de la vie quotidienne tels que les plis d'une feuille de papier
ou encore la sonnerie du téléphone. Suit alors la mise en application
sous la forme de programmes récursifs, puis la réalisation de programmes
simples.


La récursivité peut être très bien visualisée. Le programme dirigé
examine ainsi en profondeur les courbes à définition récursive, par
exemple les courbes du dragon et la courbe fractale en flocon de Koch.
Les étudiants les plus rapides pourront dans l'additum programmer des
courbes plus complexes, telles que les arbres de Pythagore ou les
figures en chou-fleur (figure 15.4). Ces courbes sont très intéressantes
et autorisent de nombreuses variantes, ce qui les rend idéales pour
l'additum. Les étudiants les plus rapides ne veulent pas simplement
qu'on leur donne de quoi s'occuper : ils souhaitent relever des défis et
résoudre des problèmes les conduisant à faire leurs propres découvertes.


<img alt="Fig. 15.4. Courbes sophistiquées dans l'additum" src="Images/15.2_figure.jpg" width="450px"/>

**Fig. 15.4** Courbes sophistiquées dans l'additum
