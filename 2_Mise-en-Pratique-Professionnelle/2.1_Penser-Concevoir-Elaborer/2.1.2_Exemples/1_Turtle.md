# Comprendre les intentions d'une activité

L'acte d'enseigner ne se résume pas à être un exerciseur, c'est-à-dire enchaîner des exercices et leurs corrigés. Ainsi, lorsqu'un enseignant conçoit une progression sur un concept donné, plusieurs activités vont se succéder pour construire les apprentissages des élèves. Il ne faut pas bien sûr réinventer la roue. Même si certaines des activités peuvent être construites _from scratch_ par l'enseignant, bien souvent on est amené à réutiliser une activité d'un collègue, d'un document ressource ou enccore une activité trouvée sur le Web.

**Avant d'être donnée aux élèves, une activité se doit toutefois d'être interrogée, questionnée.** Une activité contient des intentions de la part du concepteur qui sont le plus souvent implicites. Ainsi, avant de proposer une activité, il est nécessaire de se l'approprier en se questionnant :

    * quels objectifs en terme de connaissances et de compétences visées ;
    * quel positionnement de l'activité dans la progressivité annuelle, quel(s)les pré-requis ;
    * quels exercices "cibles" de l'activité au regard des objectifs de celle-ci ;
    * quelle durée pour l'activité ;
    * quel déroulement prévu pour l'activité ;
    * comment anticiper les démarches que les élèves vont mettre en place ;
    * comment anticiper les difficultés que vont rencontrer les élèves ;
    * quel étayage pour lever ces difficultés ;
    * comment gérer l'hétérogénéité de cette activité.


## Exercice : Analyser une activité récupérée

Vous trouverez sur le Web l'[**activité Turtle**](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/blob/master/2_Mise-en-Pratique-Professionnelle/2.1_Penser-Concevoir-Elaborer/2.1.2_Exemples/activites_turtle.pdf). Après l'avoir lu, nous vous proposons de **créer un fichier markdown**, dans votre espace de travail Git : _analyse-activite-Turtle.md_ avec les entrées suivantes : 

    * Objectifs
    * pré-requis à cette activité
    * Durée de l'activité
    * Exercices cibles
    * Description du déroulement de l'activité
    * Anticipation des difficultés des élèves
    * Gestion de l'hétérogénéïté

et de la **compléter avec votre propre analyse**.
