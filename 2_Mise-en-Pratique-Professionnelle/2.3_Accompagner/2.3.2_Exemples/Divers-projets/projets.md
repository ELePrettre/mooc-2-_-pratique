Charles Poulmaire propose à ses élèves de Première différents projets.
Les projets proposés sont :
- un morpion
- un jeu de nombres
- Isola

Préalablement les élèves ont travaillé sur [Programmation_evenementielle_cours_exercices.pdf](https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/3_Accompagner/Programmation_evenementielle_cours_exercices.pdf?inline=false) 
en utilisant la <a href="https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/3_Accompagner/graphics_isn.py" target="_blank">bibliothèque graphics_isn.py</a> et le document [Documentation_graphics_nsi.pdf](https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/3_Accompagner/Documentation_graphics_nsi.pdf?inline=false) et ont regardé la <a href="https://www.lumni.fr/video/notion-de-listes-en-informatique-et-application-aux-images-numeriques#containerType=folder&containerSlug=revisions-bac-numerique-et-sciences-informatiques-1" target="_blank">Vidéo Lumni</a>

Dans un premier temps, nous pouvons nous approprier ces différents projets et en proposer une analyse puis en discuter sur le forum. On pourra par exemple aborder certains points comme :
- les connaissances travaillées.
- le format pédagogique (durée, seul, binôme, groupe...)
- les étayages pour assurer une progression des élèves. Présent ou non. Pourquoi ?
- le calendrier (points d'étapes, rendus intermédiaires...). Présent ou non. Pourquoi ?

